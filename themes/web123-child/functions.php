<?php
/**
 *	Web123 WordPress Theme
 *
 */

// This will enqueue style.css of child theme

function enqueue_childtheme_scripts() {
	wp_enqueue_style( 'web123-child', get_stylesheet_directory_uri() . '/style.css' );
	wp_enqueue_script( 'web123', esc_url( trailingslashit( get_stylesheet_directory_uri() ) . 'js/web123-child.min.js' ) );
}
add_action( 'wp_enqueue_scripts', 'enqueue_childtheme_scripts', 100 );



function custom_login_logo() {
    echo '<style type="text/css">
        h1 a { background-image:url(https://www.web123.com.au/wp-content/uploads/2016/09/logo-retina.png) !important; }
				.login h1 a { width: 100%!important; background-size: 160px!important; height: 200px!important;}
				body {background-color: #000!important; }
    </style>';
}

add_action('login_head', 'custom_login_logo');
function the_url( $url ) {
    return 'https://www.web123.com.au/';
}
add_filter( 'login_headerurl', 'the_url' );
function wpbsearchform( $form ) {

    $form = '<form role="search" method="get" id="searchform" action="' . home_url( '/' ) . '" >
    <div class="cform2">
    <input type="text" value="' . get_search_query() . '" name="s" id="s" placeholder="Search..." /><input type="image" id="searchsubmit" src="http://www.speechtherapyalbury.com.au/i/d_button_search.png"   />
    </div>
    </form>';

    return $form;
}

add_shortcode('wpbsearch', 'wpbsearchform');